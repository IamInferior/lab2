package edu.ucsd.cs110w.temperature;

public class Fahrenheit extends Temperature {
	public Fahrenheit(float t)
	{
		super(t);
	}
	public String toString()
	{
		return getValue()+" F";
	}
	@Override
	public Temperature toCelsius() {
		float value = getValue();
		value-=32.0;
		value/=1.800;
		return new Celsius(value);
	}
	@Override
	public Temperature toFahrenheit() {
		return new Fahrenheit(getValue());
	}
	@Override
	public Temperature toKelvin() {
		float value = getValue();
		value-=32.0;
		value/=1.800;
		value+=273.15;
		return new Kelvin(value);
	}
}
