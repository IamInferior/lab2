package edu.ucsd.cs110w.temperature;

public class Celsius extends Temperature {
	public Celsius(float t)
	{
		super(t);
	} 
	public String toString()
	{
		return getValue()+" C";
	}
	@Override
	public Temperature toCelsius() {
		return new Celsius(getValue());
	}
	@Override
	public Temperature toFahrenheit() {
		float value = getValue();
		value*=1.800;
		value+=32.0;
		// TODO Auto-generated method stub
		return new Fahrenheit(value);
	}
	@Override
	public Temperature toKelvin() {
		return new Kelvin((float) (getValue()+273.15));
	}
}
