/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * TODO (all093): write class javadoc
 *
 * @author all093
 *
 */
public class Kelvin extends Temperature {
	public Kelvin(float t)
	{
		super(t);
	}
	public String toString()
	{
		return getValue()+" K";
	}
	@Override
	public Temperature toCelsius() {
		return new Celsius((float) (getValue()-273.15));
	}
	@Override
	public Temperature toFahrenheit() {
		float value = getValue();
		value-=273.15;
		value*=1.800;
		value+=32.0;
		return new Fahrenheit(value);
	}
	@Override
	public Temperature toKelvin() {
		return new Kelvin(getValue());
	}
}
